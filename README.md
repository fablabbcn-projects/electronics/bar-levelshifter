# bar-levelshifter

A simple fabable bi-directional levelshifter with a MOSFET, has the footprint to be mounted on the barduino-cnc-shield.

I2C safe. Inspired by this AN: https://cdn-shop.adafruit.com/datasheets/an97055.pdf and this versions by [Adafruit](https://www.adafruit.com/product/757) and [Pololu](https://www.pololu.com/product/2595)

One or two channels, currently only one.

## BOM 

Per channel:

1 x N-Channel Mosfet: https://www.digikey.com/product-detail/en/on-semiconductor/NDS355AN/NDS355ANCT-ND/459000

2 x Resistor 10kΩ